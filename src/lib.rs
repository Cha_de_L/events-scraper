use rayon::prelude::*;
use reqwest::Client;
use select::{document::Document, predicate::Name};
use serde_derive::Serialize;
use std::error::Error;

type BoxResult<T> = Result<T, Box<Error>>;

#[derive(Clone, Debug, Serialize)]
pub struct Event {
    id: String,
    date: String,
    lieu: String,
    ville: String,
    details: Option<EventDetails>,
}

#[derive(Clone, Debug, Serialize)]
pub struct EventDetails {
    nom: String,
    img: String,
}

pub fn client() -> Client {
    use reqwest::header;

    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::USER_AGENT,
        header::HeaderValue::from_static(
            "Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0",
        ),
    );
    headers.insert(
        header::ACCEPT_LANGUAGE,
        header::HeaderValue::from_static("fr-FR"),
    );

    Client::builder().default_headers(headers).build().unwrap()
}

pub fn fetch_page_events(client: &Client, id: &str) -> BoxResult<Vec<Event>> {
    let url = format!("https://m.facebook.com/{}/events", id);
    let html: String = client.get(&url).send()?.text()?;

    let document = Document::from(html.as_str());

    use select::predicate::Predicate;

    let events = document
        .find(Name("table"))
        .next()
        .ok_or("No table found.")?
        .find(Name("table"))
        .nth(2)
        .ok_or("No table found")?
        .find(Name("td").child(Name("div")))
        .filter_map(|elem| {
            let mut spans = elem.find(Name("span"));

            let date = spans.next()?.text();
            let lieu = spans.nth(1)?.text();
            let ville = spans.next()?.text();

            let id: String = elem
                .find(Name("a"))
                .next()?
                .attr("href")?
                .split('/')
                .nth(2)?
                .split('?')
                .next()?
                .to_string();

            Some(Event {
                date,
                lieu,
                ville,
                id,
                details: None,
            })
        })
        .collect::<Vec<Event>>()
        .par_iter()
        .map(|event| {
            let details = fetch_event_details(&client, &event.id).ok();

            Event {
                details,
                ..event.to_owned()
            }
        })
        .collect();

    Ok(events)
}

fn fetch_event_details(client: &Client, id: &str) -> BoxResult<EventDetails> {
    let html: String = client
        .get(&format!("https://m.facebook.com/events/{}", id))
        .send()?
        .text()?;

    let document = Document::from(html.as_str());

    let nom = document
        .find(Name("title"))
        .next()
        .ok_or("Failed to get page title.")?
        .text();

    let img = document
        .find(Name("img"))
        .nth(1)
        .ok_or("Failed to get event img element.")?
        .attr("src")
        .ok_or("Failed to get event img url.")?
        .to_string();

    Ok(EventDetails { nom, img })
}
