#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use events::{client, fetch_page_events, Event};

use rayon::prelude::*;

use rocket_contrib::templates::Template;
use serde_derive::Serialize;

#[get("/")]
fn index() -> Template {
    let ids = vec![
        "alternatibar.rhone",
        "l214.animaux",
        "IEETLyonCollectifCitoyenPourLeClimat",
    ];

    let client = client();

    let pages: Vec<Vec<Event>> = ids
        .par_iter()
        .filter_map(|id| fetch_page_events(&client, id).ok())
        .collect();

    #[derive(Serialize)]
    struct TemplateContext {
        pages: Vec<Vec<Event>>,
    }

    let context = TemplateContext { pages };

    Template::render("main", &context)
}

#[get("/events/<id>")]
fn events(id: &rocket::http::RawStr) -> String {
    let events = fetch_page_events(&client(), id);

    match events {
        Ok(events) => format!("{:#?}", events),
        _ => format!("No events found."),
    }
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index, events])
        .attach(Template::fairing())
        .launch();
}
